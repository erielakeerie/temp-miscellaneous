# temp-miscellaneous

## Current use
These are projects/scripts I've made over the years which are used for either backup, portability, etc. Other scripts which have limited portability even between Linux systems (e.g. it is now outdated and relied upon the GNOME 3.x). 

My current plan is to soon start moving some of the code (e.g. `gen-defaults.py`, `job-search.py`, `setup-kvm.sh`, and `spotify-track.py`) into their own projects when I have the time.
