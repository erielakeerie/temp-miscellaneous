#!/usr/bin/env python3

# adds to a yaml file of job locations
# KEYS
# -----------------
# category     : any category that the job listing says or can be added for context
#                    e.g. "restaurant", "software development", "part-time", etc.
# name         : name of place
# distance     : distance from 78705 zip code
# address      : street address
# city         : city and state
# zip_code     : zip code
# description  : description of what it is about
# website      : list of websites it case
# phone        : main phone number
# toll_free    : toll_free phone number
# is_employing : True or False if known, None if not known

# -------------------------------------------------------------------------------------
# IMPORTS

import os
import sys
import itertools as it
import yaml
import argparse

# -------------------------------------------------------------------------------------
# CLASSES FOR ARGUMENT PARSING

class StoreThreeValues(argparse.Action):
    """
    Stores three values. self.values() must be called or else an error will occur.
    """
    def __init__(self,
                 option_strings,
                 dest,
                 nargs=None,
                 const=None,
                 default=None,
                 type=None,
                 choices=None,
                 required=False,
                 help=None,
                 metavar=None):
        
        nargs_valid_list = [1, '?']
        if nargs not in nargs_valid_list:
            raise ValueError("Invalid `nargs`: `nargs` must either be " + 
                    f"{' or '.join([nargs_valid_list])}")

        super(StoreThreeValues, self).__init__(option_strings=option_strings,
                                               dest=dest,
                                               default=default,
                                               type=type,
                                               choices=choices,
                                               required=required,
                                               help=help,
                                               metavar=metavar,
                                              )
    
    @classmethod
    def init_values(self, value1, value2, value3):
        for var_value, var_name in zip([value1, value2, value3], ['value1', 'value2', 'value3']):
            setattr(self, var_name, var_value)
                    
    def __call__(self, parser, namespace, values, option_strings=None):

        if values not in [self.value1, self.value2, self.value3]:
            raise ValueError(f"'{values}' must be one of the following: "+
                    f"{', '.join(['i' for i in (self.value1, self.value2, self.value3)])}")
        
        setattr(namespace, self.dest, values)



class MakePhoneNumber(argparse.Action):
    """
    Turns phone input phone number into a standardized output for storage. 
    i.e., '1234567890' becomes '+1 (123) 456-7890' if the default country area code is initialized
    to '+1'.
    """
    
    def __init__(self,
                 option_strings,
                 dest,
                 nargs=None,
                 const=None,
                 default=None,
                 default_country_code=None,
                 type=None,
                 choices=None,
                 required=False,
                 help=None,
                 metavar=None):
        """Note: default_country_code must be a string"""

        nargs_valid_list = ['+', '*']
        if nargs not in nargs_valid_list:
            raise ValueError("Invalid `nargs`: `nargs` must either be " + 
                    f"{' or '.join([nargs_valid_list])}")

        self.default_country_code = default_country_code

        try:
            if not self.default_country_code.startswith('+'):
                default_country_code = '+' + default_country_code
        except (AttributeError, TypeError): # means default_country_code is None or is
                                            #    some other type.
            if self.default_country_code is not None:
                raise TypeError("'default_country_code' parameter must be a string.")

        super(MakePhoneNumber,self).__init__(option_strings=option_strings,
                                             dest=dest,
                                             default=default,
                                             type=type,
                                             choices=choices,
                                             required=required,
                                             help=help,
                                             metavar=metavar,
                                            )

        @classmethod
        def __call__(self, parser, namespace, values, option_strings=None):

            values_buffer = values # needed for raising errors with original values text

            def convert_alph_to_num(values : str):
                """
                Needed just in case any of the input characters are alphabetic characters.
                """
                CONVERSION_DICT = {
                        '2': ['A','B','C'],
                        '3': ['D','E','F'],
                        '4': ['G','H','I'],
                        '5': ['J','K','L'],
                        '6': ['M','N','O'],
                        '7': ['P','Q','R','S'],
                        '8': ['T','U','V'],
                        '9': ['W','X','Y','Z']
                }
                ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                new_values = ''
                for char in values:
                    try:
                        if char.upper() in ALPHABET:
                            for number, letters in CONVERSION_DICT.items():
                                if char in letters:
                                    new_values += number
                        else: # '1'.upper() -> '1' for all non-alphabetic characters
                            new_values += char
                    except AttributeError:
                        raise AttributeError(f"'{values_buffer}' must be passed in as string")

                return new_values

            # convert any letters in values to numbers
            values = convert_alph_to_num(values)
            # get rid of any character in values that isn't a digit
            number_string = ''
            for char in values:
                try:
                    int(char)
                except ValueError: # means char isn't a digit
                    pass
                else:
                    number_string += char

            if len(number_string) <= 7:
                raise argparse.ArgumentError(f"'{values_buffer}' needs an area code included.")
            elif 7 < len(number_string) < 10:
                raise argparse.ArgumentError(f"'{values_buffer}' has an invalid number of digits")
            elif len(number_string) == 10: # means it doesn't have a country code
                if self.default_country_code is not None:
                    number_string = self.default_country_code + number_string
            else: # means country code is included if len > 10
                number_string = '+' + number_string

            # final edits to number_string
            # number_string[-11::-1] yields the country code but in reverse.
            #    if country code doesn't exist, then number_string[-11::-1] -> ''
            # number_string[-8:-11:-1] yields the area code in reverse
            # likewise other slices are yielded in reverse
            # number_string[begin:end:-1][-1::-1] reverses the text again but in the right order
            number_string = number_string[-11::-1][-1::-1] + " (" + \
                    number_string[-8:-11:-1][-1::-1] + ") " + number_string[-5:-8:-1][-1::-1] + \
                    '-' + number_string[-1:-5:-1][-1::-1]
            # remove leading space if it has one
            number_string = number_string[1::] if number_string.startswith(' ') else number_string

            # FINALLY set self.dest
            setattr(namespace, self.dest, number_string)


# -------------------------------------------------------------------------------------
# ARGUMENT PARSING AND CONSTANTS           
               
# Example location under the linux file system
DEFAULT_JOB_SEARCH_YAML = os.path.join(os.environ['HOME'], "Documents", "job-search.yaml")

VERBOSITY_HIERARCHY = ['quiet', 'default', 'verbose', 'extra_verbose']
# example state
DEFAULT_STATE_OR_PROVINCE = 'Texas' 

def init_arguments(args_to_parse):
    """Initializes terminal arguments used in program."""
    # initialize
    parser = argparse.ArgumentParser(description='Adds arguments to selected location as a yaml')

    ### YAML arguments ###
    parser.add_argument('-c', '--category', metavar='string', action='extend', nargs='*',
            help='Adds to tags of general type/atmosphere of work location.')
    parser.add_argument('-n', '--name', metavar='string', nargs=1,
            help='Name of the work location.')
    parser.add_argument('-d', '--distance', metavar='int', type=int,
            help="Distance in miles/km - depending on user - from user's location.")
    parser.add_argument('-a', '--address', metavar='string', nargs=1,
            help="Address (not city, state, nor zip code) of a workplace")
    parser.add_argument('-C', '--city', metavar='string',
            help='City the workplace is located.')
    parser.add_argument('-s', '--state', '--province', metavar='string', nargs='?',
            help='State or province where workplace is located')
    parser.add_argument('-z', '--zip-code', metavar='', 
            help='Zip code of the workplace (if applicable).')
    parser.add_argument('-D', '--description', metavar='string', type=str,
            help='Description of the workplace or what it does.')
    parser.add_argument('-w', '--website', metavar='link', action='extend', nargs='+',
            help="Adds website(s) as a list.")

    # default_country_code comes from MakePhoneNumber class
    parser.add_argument('-p', '--phone', metavar='string', type=str, action=MakePhoneNumber,
            nargs='+', default_country_code='+1',
            help="Gets the phone number of the workplace.")
    parser.add_argument('-P', '--toll-free', '--toll-free-phone', metavar='string', type=str, 
            nargs='+', action=MakePhoneNumber, default_country_code='+1',
            help="Adds toll free phone number of the workplace.")

    # initialize three values for is_employing
    StoreThreeValues.init_values(True, False, None)
    parser.add_argument('-e', '--is-employing', '--employing', action=StoreThreeValues, nargs='?',
            default=None,
            help='Sets flag for if workplace is employing, None if unknown or if not called.')
    ### End YAML arguments ####

    ### Functional arguments ###
    parser.add_argument('--dry-run', action='store_true',
            help=f"Dry run of {os.path.basename(__file__)}. Should run '--verbose' as well.")
    parser.add_argument('-i', '--input', metavar='file', nargs='?',
            default=DEFAULT_JOB_SEARCH_YAML,
            help=f"Loads from input file. By default, the input is {DEFAULT_JOB_SEARCH_YAML}.")
    parser.add_argument('-o', '--output', metavar='file', nargs='?', 
            default=DEFAULT_JOB_SEARCH_YAML,
            help=f"Store yaml results in file. By default, outputs to {DEFAULT_JOB_SEARCH_YAML}.")
    # read
    parser.add_argument('--read', '--read-file', '--cat', action='store_true',
            help=f"Purely reads file. If other arguments are called, they are cancelled. By " +
                f"default, reads from {DEFAULT_JOB_SEARCH_YAML}")
    # verbosity mutually exclusive group
    verbosity = parser.add_mutually_exclusive_group()
    verbosity.add_argument('-v', '--verbose', action='count',
            help="Gives verbose output.")
    verbosity.add_argument('-q', '--quiet', action='store_true',
            help="Gives quiet output.")
    # add/update mutually exclusive group
    # this is just a crutch for now
    #add_or_update = parser.add_mutually_exclusive_group()
    #add_or_update.add_argument('-A', '--add', '--append', action='store_true',
    #        help='Append to the yaml file')
    #add_or_update.add_argument('-U', '--update', action='store_true',
    #        help='Update entry in yaml file')
    ### End Functional arguments ###
    return parser.parse_args(args_to_parse.split(''))

def interpret_verbosity_from_args(args):
    """
    Requires parser.parse_args() to be instantiated.
    ::: -q, --quiet   --> 'quiet'
    ::: None passed   --> 'default'
    ::: -v, --verbose --> 'verbose'
    ::: -vv           --> 'extra_verbose'
    """
    # vars are given in the correct hierarchical order
    quiet, default, verbose, extra_verbose = [i for i in VERBOSITY_HIERARCHY]
    if args.quiet:
        return quiet
    elif args.verbose == 1:
        return verbose
    elif args.verbose > 1:
        return extra_verbose
    else:
        return default


# -------------------------------------------------------------------------------------
# YAML-RELATED I/O FUNCTIONS

def init_yaml_data():
    """Returns basic yaml structure the rest of the program will expect to work with."""
    return { 'workplaces': [] }

def yaml_load(filepath):
    """Loads from yaml file. Returns corresponding dictionary."""
    with open(filepath, 'r') as f:
        return yaml.load(f, Loader=yaml.FullLoader)

def yaml_dump(data : dict, filepath, part_changed=None, dry_run=False, verbosity='default'):
    """
    Dumps data to yaml file if dry_run is False.
    Verbosity can only be 'quiet', 'default', 'verbose', 'extra_verbose'.
    Part_changed refers to the part of the yaml file that was created or updated.
    """
    if not dry_run:
        with open(filepath, 'w') as f:
            yaml.dump(data, f, default_flow_style=False)

    # One of these three should be true.
    # VERBOSITY_HIERARCHY = ['quiet', 'default', 'verbose', 'extra_verbose']
    quiet, default, verbose, extra_verbose = \
            [ True if verbosity == i else False for i in VERBOSITY_HIERARCHY ]
    if quiet:
        pass
    if default:
        if dry_run:
            print("No errors occurred.")
        else:
            print(f"Results saved to {filepath}.")
    if verbose:
        yaml.dump(part_changed, sys.stdout, default_flow_style=False)
    if extra_verbose:
        yaml.dump(data, sys.stdout, default_flow_style=False)


# -------------------------------------------------------------------------------------
# __main__

if __name__ == '__main__':

    args = init_arguments()

    yaml_data = yaml_load(args.input)
    if yaml_data is None:
        yaml_data = init_yaml_data()

    if args.read:
        yaml_dump(yaml_data, args.output, dry_run=True, verbosity='extra_verbose')
        sys.exit(0)

    if not any(args.phone):
        raise argparser.ArgumentError("A phone number needs to be initialized for comparison purposes")

    yaml_priority_queue = {
            'name'        : args.name,
            'category'    : args.category,
            'distance'    : args.distance,
            'address'     : args.address,
            'city'        : args.city,
            'state'       : args.state,
            'zip_code'    : args.zip_code,
            'phone'       : args.phone,
            'toll_free'   : args.toll_free,
            'website'     : args.website,
            'description' : args.description,
            'is_employing': args.is_employing
    }

    current_yaml_data = {}

    for key_name, arg in yaml_priority_queue.items():
        current_yaml_data[key_name] = arg

    yaml_data['workplaces'].append(current_yaml_data)

    yaml_dump(yaml_data, args.output, 
              part_changed=current_yaml_data,
              dry_run=args.dry_run,
              verbosity='verbose')

    
    """"
    def key_exists(key, dictionary):
        try:
            dictionary[key]
        except KeyError:
            return False
        else:
            return True

    def return_workplace_aspects(aspect : str):
        to_return = []
        for workplace in yaml_data['workplaces']:
            if key_exists(aspect, workplace):
                to_return.append(workplace[aspect])
            else:
                to_return.append(None)
        return to_return
    """
    # TODO: work on being able to update certain yaml entries

    """
    def _xor(*args):
        
        args_list = [arg for arg in args]
        # this gives for what
        wanted_combs = list(it.combinations(args_list, len(args_list)-1))
        for comb in wanted_combs:
            pass 

        
        
        

    def return_matching_indices(main_iter, *iters_to_compare):
        pass
    

    workplace_names = return_workplace_aspect('name')
    workplace_addresses = return_workplace_aspect('address')
    workplace_phone_nums = return_workplace_aspect('phone')
    workplace_toll_free = return_workplace_aspect('toll_free')
    workplace_zip_codes = return_workplace_aspect('zip_code')
    """
     
                
    

        









