#!/usr/bin/env bash

# This script is for being able to dynamically scale the display 
# in non-GNOME desktop environments in a QEMU/KVM virtual machine.
# This is to be run in the guest virtual machine ONLY.
# 
# NOTE: This is only tested on virtual machines using X11.
#       I do not imagine this would work on Wayland.
# 
# By: Erie Shadrock

RESIZE_NAME="x-resize"
XSESSION_NAME="999xresize"

# Create resize script and place in /usr/local/bin/ directory.
sudo cat << EOF > "/usr/local/bin/$(echo $RESIZE_NAME)"
#!/usr/bin/env bash

# This script automatically scales the display with use of xrandr
# for scaling and with use of xev for monitoring for changes.
# NOTE: This script hasn't been tested with more than 1 virtual monitor.
# by: Erie Shadrock

function scale_display () {
	# The "xrandr | awk" code should output "Virtual-1" or something similar
	# i.e. the main display of the virtual machine
	
	xrandr --output "$(xrandr | awk '/ connected/{print $1; exit; }')" --auto
}

# sleeping for 2 seconds is mostly just cautionary
sleep 2

# scale display once before entering loop 
# (otherwise it takes a bit of time for first scaling)
scale_display

xev -root -event randr | \
	grep --line-buffered 'subtype XRROutputChangeNotifyEvent' | \
	while read foo ; do
		scale_display
	done
EOF

# make script executable
sudo chmod +x "/usr/local/bin/$(echo $RESIZE_NAME)"

echo "File successfully made: /usr/local/bin/$(echo $RESIZE_NAME)"

# now create script in /etc/X11/Xsession.d/
# which enables automatic resizing upon startup
# needs spice-vdagent to execute resize script

sudo cat <<EOF > "/etc/X11/Xsession.d/$(echo $XSESSION_NAME)"
#!/usr/bin/env bash

if [ -x /usr/bin/spice-vdagent ]; then
	/usr/bin/spice-vdagent
	"/usr/local/bin/$(echo $RESIZE_NAME)" &
fi

exec "/usr/bin/$(echo $XDG_SESSION_DESKTOP)-session"
EOF

# make xsession script executable
chmod +x "/etc/X11/Xsession.d/$(echo $XSESSION_NAME)"

echo "File successfully made: /etc/X11/Xsession.d/$(echo $XSESSION_NAME)"
sleep 0.5
echo "Changes will take place upon next boot."





