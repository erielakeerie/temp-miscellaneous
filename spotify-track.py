#!/usr/bin/env python3

# Creates a local database of artists and albums along with relevant IDs


# -------------------------------------------------------------------------------------
# IMPORTS

import json, argparse, subprocess, pprintpp, time, platform, os, requests
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from pprintpp import pprint
import spotipy.oauth2 as spo
from PIL import Image, ImageShow

# -------------------------------------------------------------------------------------
# CONSTANTS

CLIENT_ID = "YOUR_CLIENT_ID"
CLIENT_SECRET = "YOUR_CLIENT_SECRET"
REDIRECT_URI = 'http://localhost:8000/callback'

# os.path.basename(__file__) returns the file name of the python program
# .replace(' ', '-') replaces any potential spaces with '-'
# .split('.') splits the file name into two if it ends with '.py'
DATABASE_NAME = f"{os.path.basename(__file__).replace(' ', '-').split('.')[0]}_database.json"
APP_FOLDERNAME = f"{os.path.basename(__file__).replace(' ', '-').split('.')[0]}"


ARTISTS_DATABASE = "artists-database.json"

# right now used as a reference
ARTIST_TEMPLATE_DATA = {
    'artist_name': {
        'name': '',
        'id': '',
        'albums': [ {
            'name': '',
            'id': '',
            'tracks': [ {
                'order': '',
                'disc': '',
                'name': '',
                'id': ''
            } ]
        } ],
        'singles': [ {
            'name': '',
            'id': '',
            'tracks': [ {} ]
        } ]
    }
}


sp = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials(
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET
))

# -------------------------------------------------------------------------------------
# BASE FUNCTIONS AND CLASSES

def _add_if(element, container, any_true=False, num_true=None, all_true=False,
        add_key_value_pairs=False,
        *conditions : bool):
    """
    Adds element to list or dict only if selected flag is resolves to True.
    All conditions must resolve to a boolean. If container is neither a list nor dict, it will be
    assumed to be container is dict[key].
    Main flags. Returns True if element successfully added to list and False if not.
        - any_true: at least 1 condition must be true -> True
        - all_true: all conditions must be true -> True
        - num_true: x and only x conditions must be true -> True
    Interactions:
        - num_true + any_true: at least x or more conditions must be true -> True
        - num_true + all_true: less than x conditions must be true -> True
    Misc. Flags:
        - update_key_value_pairs: If True, element will be turned into a dict if not already,
            and key-value pairs will either be added/updated in container.
    """
    def num_conditions_true(*args):
        """Returns the number of conditions that are true along with the number of conditions
        total."""
        total = 0
        count = 0
        for arg in args:
            if arg == Tr/ue:
                total += 1
            count += 1
        return total, count

    def add_element(element, container, add_key_value_pairs=add_key_value_pairs):
        """Adds elements to containing class: either a list or dict. Element must be a list"""
        if type(container) == list:
            container.append(element)
        elif type(container) == dict:
            if add_key_value_pairs == True:
                for key, value in dict(element).items():
                    container[key] = value
            else: # update dictionary as normally
                pass # idk what to do for this right now
                     # I might get rid of add_key_value_pairs and change the documentation
                     #   accordingly.
        else: # assumed that a key in a dictionary is being updated
            container = element
    
    # actual start of main '_add_if' function
    num_conds_true, num_conds = num_conditions_true(*conditions)

    # this is to make num_true work with the upcoming if-elif-else chain
    # to avoid a TypeError
    if num_true is None:
        num_true = -1

    if all([num_true >= 0, any_true, all_true]):
        raise Exception("Not all 'any_true = True', 'all_true = True', and " +
                "'num_true = positive_integer' can occur at once")
    elif num_true >= 0 and any_true == True:
        if num_conds_true >= num_true:
            add_element(element, container, add_key_value_pairs=add_key_value_pairs)
            return True
    elif num_true >= 0 and all_true == True:
        if num_true < num_conds_true < num_conds:
            add_element(element, container, add_key_value_pairs=add_key_value_pairs)
            return True
    elif num_true:
        if num_conds_true == num_true:
            add_element(element, container, add_key_value_pairs=add_key_value_pairs)
            return True
    elif any_true:
        if num_conds_true > 0:
            add_element(element, container, add_key_value_pairs=add_key_value_pairs)
            return True
    elif all_true:
        if num_conds_true == num_conds:
            add_element(element, container, add_key_value_pairs=add_key_value_pairs)
            return True
    else: # if none of the flags are set
        raise Exception("At least one of the flags 'any_true', 'all_true', 'num_true' " +
                    "must be explicitly set.")
    
    return False


def _add_if_not_exists(element, _list : list):
    """Adds and element if it does not exist in list."""
    _add_if(element, _list, all_true=True, conditions=element not in _list)


def init_arguments():
    parser = argparse.ArgumentParser(description='TODO: make description')
    # arguments
    artist_args = parser.add_mutually_exclusive_group()
    artist_args.add_argument('-a', '--artist', metavar='artist',
            help='Select artist in database')
    artist_args.add_argument('-A', '--add', '--append', metavar='artist', 
            help='Add artist result to local spotify database')

    discography_args = parser.add_mutually_exclusive_group()
    discography_args.add_argument('-l', '--list-discography', metavar='discography_subset', 
            help="Lists all albums, singles, and EPs of an artist")
    discography_args.add_argument('--list-albums', metavar='discography_subset', 
            help="Lists all albums of an artist")
    discography_args.add_argument('--list-singles', '--list-eps', metavar='discography_subset',
            help="List all singles and EPs of an artist")
    discography_args.add_argument('--list-songs', metavar='discography_subset',
            help="List all songs of an artist (along with albums, singles, and EPs they belong to)")
    discography_args.add_argument('-u', '--update', metavar='discography_subset', 
            help="Updates entire discography of selected artist(s). If no artist is passed, then it updates all.")

    parser.add_argument('--dry-run', metavar='',
            help='Does not append anything to spotify database')

    verbosity_args = parser.add_mutually_exclusive_group()
    verbosity_args.add_argument('-q', '--quiet', action='store_true',
            help='Quiet output')
    verbosity_args.add_argument('-v', '--verbose', action='store_true',
            help='Verbose output')

    return parser.parse_args()


def _DEFAULT_DATABASE_FILEPATH(same_app_dirpath=True, database_name=DATABASE_NAME):
    """
    Returns a default database file path depending on OS. If `same_app_dirpath` is True, then it
    OS-agnostically stores the database file in the same directory as the python program.
    - Windows: %USERPROFILE%\AppData\Roaming\{program_name}\{database_name}
    - MacOS/OSX: ~/Library/Application Support/{program_name}/{database_name}
    - Linux: ~/.config/{program_name}/{database_name}
    """

    if same_app_dirpath == True:
        app_dirpath = os.path.dirname(__file__)
        return os.path.join(app_dirpath, database_name)
    
    sys_platform = os.platform.system().lower()
    if sys_platform == "windows":
        database_dirpath = os.path.join(os.environ['USERPROFILE'], "AppData", "Roaming", 
                APP_FOLDERNAME)
    elif sys_platform == "darwin":
        database_dirpath = os.path.join(os.environ['HOME'], 'Library', 'Application Support',
                APP_FOLDERNAME)
    elif sys_platform == "linux":
        database_dirpath = os.path.join(os.environ['HOME'], ".config", APP_FOLDERNAME)
    else:
        raise OSError("OS not supported: cannot define location of database folder")
    
    return os.path.join(database_dirpath, database_name)


        



def get_spotify_client_id(id_type):
    """Returns a spotify ID value which is part of the global bash variables of the system."""
    id_type = id_type.upper()
    valid_id_types = ["SPOTIFY_CLIENT_ID", "SPOTIFY_CLIENT_SECRET"]
    if id_type in valid_id_types:
        process = subprocess.run(f"echo ${id_type}", shell=True, capture_output=True, text=True)
        id_value = process.stdout
        return id_value
    else:
        raise ValueError(f"'{id_type}' is not correct value. ID Type must be one of the following:\n\t'{', '.join(valid_id_types)}")


# -------------------------------------------------------------------------------------
# MAIN PROGRAM FUNCTIONS

def get_artist_image(artist_name : str, artist_dict : dict, 
        size='large', 
        save_directory=None,
        save_name=None,
        parse_args=False):
    """
    Shows artist image given a link. Size can be:
        - small : typically shows ~160x160 or 64x64 image
        ::: - picks minimal size available
        - medium: typically shows ~320x320 image
        ::: - picks median size available (rounded up to be usable)
        - large : typically shows ~640x640 image
        ::: - picks largest size available
    Other parameters:
        - save_directory : can either be input as a list, tuple, or string
        ::: - must be file path ONLY
        ::: - defaults to OS's temp directory if no argument is passed
        - save_name : name of image
        ::: - must not any part of a file path
        ::: - defaults to {artist_name}_profile-pic.jpeg if no argument is passed
        - parse_args : calls upon init_arguments() and args therein if True
        ::: - init_arguments() = argparse.ArgumentParser().parse_args()
    """
    if parse_args:
        args = init_arguments()

    def valid(argument : bool) -> bool:
        """
        Used for args.quiet and args.verbose. Returns True if args.{argument} exists and is True.
        """
        try:
            argument
        except (AttributeError, NameError):
            return False
        else:
            return bool(argument)


    try:
        image_list = artist_dict['images']
    except KeyError as error:
        raise KeyError(f"{error} key not found in dictionary passed into function.")
    
    size = size.lower()
    accepted_sizes = ['small', 'medium', 'large']
    small, medium, large = (i for i in accepted_sizes)

    dimensions_list = [ ( image_list[i]['width'], image_list[i]['height'] ) for i in \
        range(len(images_list)) ]
    weighted_dimensions = []
    
    for width, height in dimensions_list:
        # quadratically favors more square-shaped objects
        # note that max or min(x, x) = x for any real number x, i.e. is reflexive,
        #       which is useful in the case height = width.
        weighted_dimension = width * height * (max(width,height) / min(width,height)) ** 2
        weighted_dimensions.append(weighted_dimension)
    
    # get needed index
    if size == small or size == large:
        weighted_dimensions_sorted = sorted(weighted_dimensions, 
                                            reverse=False if size == small else True)
        desired_dimensions = weighted_dimensions_sorted[0]

    elif size == medium:
        weighted_dimensions_sorted = sorted(weighted_dimensions)
        # manually compute necessary median so as to not have to import as few unnecessary modules
        # as possible. Note that our approximate median has to correspond to a number belonging to
        # weighted_dimensions_sorted.
        while len(weighted_dimensions_sorted) > 2:
            weighted_dimensions_sorted.pop(0)
            weighted_dimensions_sorted.pop(-1)

        if len(weighted_dimensions_sorted) == 2:
            desired_dimensions = weighted_dimensions_sorted[1]
        else: # weighted_dimensions_sorted has 1 element 
            desired_dimensions = weighted_dimensions_sorted[0]

    else: # means size not in accepted_sizes
        raise ValueError(f"'{size.lower} is not among accepted sizes of: " + 
            f"{', '.join('i' for i in accepted_sizes)}")

    for index in range(len(dimensions_list)): # doesn't matter the exact list called as long as
                                              # there exists a 1-to-1 and onto correspondence
        if desired_dimensions == weighted_dimensions[index]:
           desired_index = index
           break

    desired_url = image_list[desired_index]['url']

    if valid(args.verbose):
        print(f"Dimensions of size {'x'.join(dimensions_list[desired_index])} chosen.")
    
    try:
        # want this explicitly as a ternary in order to throw a TypeError for objects that
        # are not a tuple, list, nor string
        save_directory = save_directory if type(save_directory) == str \
                else os.path.join(*save_directory)
    except TypeError: # means save_directory is NoneType
        temp_foldername = os.path.basename(__file__)
        sys_platform = platform.system().lower()
        if sys_platform == "windows":
            temp_path = os.path.join(os.environ['USERPROFILE'], "AppData", "Local", "Temp")
        elif sys_platform in ["darwin", "linux"]:
            temp_path = "/tmp"
        else:
            raise OSError(f"'{platform.system()}' is not currently supported.")
        save_directory = os.path.join(temp_path, temp_foldername)

    if save_name is None:
       save_name = f"{artist_name}_profile.jpeg"

    save_fullpath = os.path.join(save_directory, save_name)

    try:
        desired_request = requests.get(desired_url)
    except requests.HTTPError as error:
        raise requests.HTTPError(error)

    with open(save_fullpath, 'wb') as desired_image:
        desired_image.write(desired_request.content)
    
    if valid(args.quiet): # no output
        pass
    elif valid(args.verbose):
        print(f"Response {desired_request.status_code} from '{desired_url}': " + \
              f"{save_name} saved ")
    else: # default
        print(f"Response {desired_request.status_code}: image retrieved.")

    return save_fullpath

    


        
class _EogViewer2(ImageShow.UnixViewer):
    """
    It's almost identical to ImageShow.EogViewer except the file isn't removed in `show_file`.
    """
    def show_file(self, file, **options):
        """Display file and set debugging in terminal to off."""
        subprocess.Popen(["eog", "-n", file, "--gtk-no-debug=True"])
        return



def _show_image(image_path : str):
    """
    Different than PIL.Image.open().show(). Prioritizes on Linux showing image through Eye of 
    Gnome (eog) if it exists. imagemagick is prioritized over eog if it is also installed.
    """
    #ImageShow.register(_EogViewer2, order=0) # doing this and then Image.open(image_path).show()
                                              #    gives me a buggy duplicate for some reason.
                                              #    absolutely no idea why.
                                              # using ImageShow.EogViewer in the register function
                                              #    is also buggy. no idea. it just redirects to a
                                              #    tmp file which doesn't exist.
    try:
        _EogViewer2().show_file(image_path)
    except FileNotFoundError: # no Eye of Gnome (eog) on system
        image = Image.open(image_path)
        image.show()
    
    
    

def interactive_search_artist(name : str):
    """Gives interactive response based upon search name"""
    limit = 10 # gives search limit for each page of results
    current_offset = 0

    while True:
        search_result = sp.search(q=f'artist:{name}', type='artist', limit=limit, 
            offset=current_offset)
        # search_result['items'] is a list
        potential_artists = [ item['name'] for item in search_result['items'] ]
        num_artists = len(potential_artists)
        print(f"Here are the potential artists for the search '{name}':", "\n", sep='')

        for num, artist in enumerate(potential_artists + ['Go to next list of artists...'], 
            start=1):
            print(f"\t{num}) {artist}")

        print('')
        chosen_artist_num = ''

        while chosen_artist_num is not type(int):
            chosen_artist_num = input(f"Which result best fits your search (0 to abort, " + 
                    f"{num_artists+1} for next)? [0-{num_artists+1}]: ")
            try:
                chosen_artist_num = int(chosen_artist_num)
            except ValueError:
                continue
            else:
                if not 0 <= chosen_artist_num <= num_artists+1:
                    continue

        if chosen_artist_num == 0:
            import sys
            sys.exit(1)
        elif chosen_artist_num == num_artists + 1:
            current_offset += num_artists
            continue

        chosen_artist = potential_artists[chosen_artist_num-1]
        confirm_artist = None
        valid_responses = ['y', 'n', 's']     # 's' for show picture of artist,
                                              #     must remain at index 2
        default_response = valid_responses[0] # shows up as 'Y' in input string

        while confirm_artist not in [ valid_responses[0], valid_responses[1] ]:
            # top line in .join statement is a ternary
            # shows up as 'Y/n/s' in current code
            confirm_input_str = '/'.join([i if i != default_response else i.upper() \
                                          for i in valid_responses])
            confirm_artist = input(f"Is '{chosen_artist}' correct (use '{valid_responses[2]}' to" +
                f" show artist pic)? [{confirm_input_str}]: ")
            confirm_artist = confirm_artist.lower()
            if confirm_artist == '':
                confirm_artist = default_response
            if confirm_artist == valid_responses[2]:
                print(f"Retrieving image of {chosen_artist}...")
                artist_image = get_artist_image(chosen_artist, 
                        artist_dict=search_result['items'][chosen_artist_num-1],
                        size='large', parse_args=True)
                print("Showing image in an external window...")
                time.sleep(1)
                _show_image(artist_image)
                
        if confirm_artist == valid_responses[1]: # i.e. is 'n'
            time.sleep(1)
            print('')
            continue

        # by process of elimination, confirm_artist = 'y'
        # TODO: implement add_artist_data and make interactive process for that
        

        


        
        
def add_artist_data(artist_name : str, update_old=False,
        add_albums=False, 
        add_singles=False, 
        add_appears_on=False,
        add_compilations=False):
    """
    Adds artist data to the database. The dictionary is structured as such:
    artist_dict = {
        'artist_name': {
            'id': '',
            'albums': [ {
                'name': '',
                'id': '',
                'release_date': '',
                'release_date_precision: '',
                'tracks': [ {
                    'order': '',
                    'disc': '',
                    'name': '',
                    'id': ''
                } ]
            } ],
            'singles': [ {
                'name': '',
                'id': '',
                'release_date': '',
                'release_date_precision': '',
                'tracks': [ {
                    'order': '',
                    'disc': '',
                    'name': '',
                    'id': ''
                } ]
            'appears_on' : ... (same)
            'compilation' : ... (same)
            } ]
        }
    }

    """

    def key_exists(key, dct : dict):
        """Checks if key exists in dictionary. Returns True if yes and False if no."""
        try:
            dct[key]
        except KeyError:
            return False
        else:
            return True


    DATABASE_FILEPATH = _DEFAULT_DATABASE_FILEPATH()
    
    function_vars = locals() # returns this function's vars and values as dict
    datatype_to_add = [ datatype for datatype, truth_value in function_vars.items() \
            if truth_value == True ]
    datatype_to_add.pop(0) # get rid of update_old
    if not any(datatype_to_add):
        wanted_vars = list(function_vars.keys())[1:]
        raise Exception(f"At least one of {', '.join('i' for i in wanted_vars)}" + 
                "must be set to True")

    if not os.path.exists(DATABASE_FILEPATH):
        base_json_string = "{ 'artists' : [] }"
        database_dict = json.loads(base_json_string)
        with open(DATABASE_FILEPATH, 'w') as f:
            json.dump(database_dict, f, indent=2)
        

    with open(DATABASE_FILEPATH, 'r') as f:
        database_dict = json.load(f)

    try:
        artist_dict = database_dict['artists'][artist_name]
    except KeyError: # artist name does not yet exist in database
        artist_dict = ARTIST_TEMPLATE_DATA
        artist_dict[artist_name] = artist_dict['artist_name']
        del artist_dict['artist_name']

    # get artist spotify id
    current_offset = 0
    while not key_exists('id', artist_dict[artist_name]):
        artist_search_dict = sp.search(q=f'artist:{artist_name}', type='artist', 
                offset=current_offset)
        for item in artist_search_dict['artists']['items']:
            # might cause a bug if there are multiple artists with the exact same name
            if artist_name == item['name']:
                artist_dict[artist_name]['id'] = item['id']
                break
        else:
            num_items = len(artist_search_dict['artists']['items'])
            current_offset += num_items
    
    artist_id = artist_dict[artist_name]['id']

    # make datatype_queue compliant with spotify api
    datatype_list = [ datatype[4:] for datatype in datatype_to_add ] # removes 'add_'
                                                                     # need this list for use in 
                                                                     #    add_artist_datatype()
    # remove 's' if datatype ends with 's' else do nothing
    datatype_queue = [ datatype[:-1] if datatype.endswith('s') else datatype \
            for datatype in datatype_queue ]


    
    def add_artist_tracks(album_id : str, artist_album_dict : dict):
        """Adds artist tracks to artist_album_dict"""
        limit = 30 # limit amount of item in items of the search
        current_offset = 0
        dict_toparse = sp.album_tracks(album_id, offset=current_offset, limit=limit)
        limit = 30


        
        





    def add_artist_datatype(datatype : str):
        """Adds artist data type. Must be 'albums', 'single', 'appears_on', or 'compilation'."""
        # database_keyname is describes how the key appears in the local database
        datatype_keyname = [ i for i in datatype_list if i == datatype ][0]

        dict_toparse = sp.artist_albums(artist_id, album_type=datatype)
        num_datatype = dict_toparse['total']
        current_offset = 0
        
        # initialize if it doesn't exist
        if not key_exists(datatype_keyname):
            artist_dict[artist_name][datatype_keyname] = []

        while current_offset < num_datatype:
            for item in dict_toparse['items']:
                dict_to_add = { 
                        'name': item['name'],
                        'id': item['id'],
                        'release_date': item['release_date'],
                        'release_date_precision': item['release_date_precision']
                }
                # if datatype_keyname dict not empty
                if len(artist_dict[artist_name][datatype_keyname]) > 0:
                    # TODO: make the _add_if code more understandable if not rip it out entirely
                    # add if item['name'] does not already exist in artist_dict
                    # conditions in this context are a set of booleans 
                    conditions = [ 
                            *( item['name'] != i for i in \
                                artist_dict[artist_name][datatype_keyname]['name'] ),
                            update_old == False
                    ]
                    if _add_if(dict_to_add, artist_dict[artist_name][datatype_keyname],
                                all_true=True,
                                conditions=conditions):
                        continue # means _add_if added 'dict_to_add' successfully
                                 # no need to call the next function
                                 # TODO: add tracks
                    # add if item['name'] exists and update_old == True
                    # it will update the key-value pairs that exist within 'dict_to_add'
                    conditions = [
                            *( item['name'] == i and update_old == True for i in \
                                artist_dict[artist_name][datatype_keyname] )
                    ]
                    if _add_if(dict_to_add, artist_dict[artist_name][datatype_keyname],
                            num_true=1, add_key_value_pairs=True,
                            conditions=conditions):
                        pass # want to add tracks
                else: # if empty
                    artist_dict[artist_name][datatype_keyname].append(dict_to_add)
            else: # the else statement will always run
                  # this is purely for my own organizational purposes
                # now add to current_offset
                offset_to_add = dict_toparse['limit']
                current_offset += offset_to_add


                    
                    

                    



                            

                            
                    
                    
                    
                


        

        



        

            

    


def initialize_artist(name, artist_id=None):
    artist_dict = ARTIST_TEMPLATE_DATA
    try:
        pass
    except: pass


# all testing below
def main():
    example_artist = 'aurora'
    my_search = sp.search(q=f"artist:{example_artist}", type='artist')
    pprint(my_search)

def main2():
    test_artist = 'AURORA'
    test_id = '1WgXqy2Dd70QQOU7Ay074N'
    my_search = sp.artist_albums(test_id, album_type='album', limit=10, offset=0)
    pprint(my_search)

lst = [ i for i in range(10) ] 
def main3():
    # Testing David Bowie's Brillant Adventure (1992-2001)
    # has 131 songs for testing
    test_artist = 'David Bowie'
    test_artist_id = '0oSGxfWSnnOXhD2fKuz2Gy'
    test_album = 'Brilliant Adventure (1992 - 2001)'
    test_album_id = '0zL2nmuAvTxYOdQ5iqTTIV'
    results = sp.album_tracks(test_album_id, limit=25, offset=0)
    pprint(results)

if __name__ == '__main__':
   args = init_arguments()
   main3()
   """test_url = "https://i.scdn.co/image/ab6761610000e5eb1cf32251011d049c26855ae4"
   test_artist, test_id = "AURORA", "1WgXqy2Dd70QQOU7Ay074N"
   my_image = requests.get(test_url)
   with open('/tmp/test.jpeg', 'wb') as f:
       f.write(my_image.content)
   _show_image('/tmp/test.jpeg')"""
   


    
