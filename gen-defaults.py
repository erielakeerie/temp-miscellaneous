#!/usr/bin/env python3

# This program is meant to generate default templates
# Generates templates of...:
#   - makefiles
#   - markdown

import argparse

parser = argparse.ArgumentParser(description="Creates default templates.")
# Template Arguments
parser.add_argument('-m', '--markdown', type=str, metavar='template name', 
    help="Generates default markdown file")
parser.add_argument('-M', '--makefile', type=str, metavar='template name', 
    help="Generates default make file")

# Functional Arguments
parser.add_argument('-o', '--output', type=str, metavar='file', 
    help='Specify the output location/name of the file.')
parser.add_argument('-t', '--template', type=str, metavar='file',
    help='Specify the location of the template.')

# Verbosity Arguments
group_verbosity = parser.add_mutually_exclusive_group()
group_verbosity.add_argument('-q', '--quiet', action='store_true', 
    help='Prints quiet output')
group_verbosity.add_argument('-v', '--verbose', action='store_true', 
    help='Prints detailed, verbose output.')

args = parser.parse_args()

if __name__ == "__main__":
    
    if not any(args.markdown, args.makefile):
        raise argparse.ArgumentError("No template arguments called")
